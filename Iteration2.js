// 2.1 Dado el siguiente javascript y html. Añade la funcionalidad necesaria usando 
// fetch() para hacer una consulta a la api cuando se haga click en el botón, 
// pasando como parametro de la api, el valor del input.

const baseUrl = 'https://api.nationalize.io';


const btnListener = ()=>{
    const input = document.getElementById('input')
    let name = input.value;
    
   fetch(`${baseUrl}?name=${name}`)
   .then((response) => response.json())
   .then((myJson)=> {
      return console.log(myJson)
   })
}

const getButton = () =>{
    const button = document.getElementById('btn')
    button.addEventListener('click', btnListener)

}



window.onload = () => {
    getButton(baseUrl)
    
}